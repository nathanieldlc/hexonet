// eslint-disable-next-line arrow-body-style
function domainNameFormatter(value) {
  return {
    Property: {
      Reason: value.PROPERTY.REASON,
    },
    Description: value.DESCRIPTION,
    Queuetime: value.QUEUETIME,
    Code: value.CODE,
    Runtime: value.RUNTIME,
  };
}

exports.domainNameFormatter = domainNameFormatter;
