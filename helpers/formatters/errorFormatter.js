// eslint-disable-next-line arrow-body-style
const errorFormatter = ({ msg, param, value }) => {
  return { parameter: param, description: { message: msg, input: value } };
};

exports.errorFormatter = errorFormatter;
