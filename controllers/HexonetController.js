async function main() {
  const apiconnector = require('@hexonet/ispapi-apiconnector')
  const cl = new apiconnector.APIClient();
  // Use OT&E system, omitting this points by default to the LIVE system
  cl.useOTESystem()
    // Set your user id, here: the OT&E demo user
    .setCredentials('test.user', 'test.passw0rd')
    // Set Remote IP Address (in case of IP Filter setting)
    .setRemoteIPAddress('1.2.3.4:80');
  // Set a subuser view
  // .setUserView('hexotestman.com')
  // Set a one time password (active 2FA)
  // .setOTP('12345678')
  
  const r = cl.request({
    COMMAND: 'StatusUser',
  });

  console.log(r.getPlain());
}
