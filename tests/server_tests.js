const request = require('supertest');
const app = require('../app.js');

describe('Server Tests', () => {
  describe('Server is running', () => {
    it.only('Check server status', (done) => {
      request(app)
        .get('/')
        .expect(200, done);
    });

    // TODO: Fix the console output on test
    it.only('GET a 404', (done) => {
      request(app)
        .get('/errore')
        .expect(404)
        .end((err) => {
          if (err) return done('Error Received');
          return done();
        });
    });

    it.only('POST[\'/json\']', (done) => {
      request(app)
        .post('/json')
        .set('Content-Type', 'application/json; charset=utf-8')
        .send({
          param1: 'test',
          param2: 'test2',
        })
        .expect('Content-Type', 'application/json; charset=utf-8')
        .expect(200, {
          param1: 'test',
          param2: 'test2',
        }, done);
    });
  });
});
