/* eslint-disable quote-props */
const express = require('express');
const apiconnector = require('@hexonet/ispapi-apiconnector');
const { check, validationResult } = require('express-validator/check');

const cl = new apiconnector.APIClient();
const router = express.Router();

/* GET home page. */
router.post('/check', [

], (req, res) => {
  res.contentType('application/json');
  res.set('Content-Type', 'application/json');

  const { domain } = req.body;

  cl.setCredentials(process.env.HEXONET_USER, process.env.HEXONET_PASS)
    .setRemoteIPAddress('epp.ispapi.net:700');

  const r = cl.request({
    COMMAND: 'CheckDomain',
    DOMAIN: domain,
  });

  r.then((value) => {
    console.log(value);
    res.render('index', {
      title: value.hash.CODE,
      description: value.hash.DESCRIPTION,
    });
  });
});


module.exports = router;
