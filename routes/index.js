/* eslint-disable quote-props */
const express = require('express');

const router = express.Router();


/* GET home page. */
router.get('/', (req, res) => {
  res.render('index', {
    title: 'GET[\'/\']',
    description: '200',
  });
});

router.get('/error', (req, res) => {
  res.render('error', {
    message: 'Error Message',
    error: {
      status: 'Error Status',
      stack: 'Error Stack',
    },
  });
});

router.post('/json', (req, res) => {
  res.contentType('application/json');
  res.set('Content-Type', 'application/json');

  const { param1, param2 } = req.body;
  const json = {
    'param1': param1,
    'param2': param2,
  };

  res.status(200)
    .json(json);
});

module.exports = router;
